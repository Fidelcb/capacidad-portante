
rm(list=ls())
capacidad_carga<-function(C=0.25,fi=29.4,Y=1.81,Z=1.2,NF=5,Beta=0,fib=1,BL=0.75,FS=1.5,Cap_p="Hansen",FSB=FALSE,fig=FALSE){
################################################################################################

q=ifelse(NF<Z,NF*Y+(Z-NF)*(1.13*Y-1),Y*Z)
Nq=exp(pi*tan(fi*(pi/180)))*tan(pi/4+(fi/2)*(pi/180))^2
Nc=ifelse(fi==0,5.14, (Nq-1)/tan(fi*(pi/180)))
Ny_H=1.5*(Nq-1)*tan(fi*(pi/180))
Ny_V=2*(Nq+1)*tan(fi*(pi/180))
##################################################################
B<-NULL
B[1]<-1
i=1
while (B[i]<3.5) {
  B[i+1]<-B[i]+0.25
  i<-i+1

}
################################################################
ypri=ifelse(NF<Z,1.13*Y-1,ifelse(NF-Z<max(B),1.13*Y-1+(NF-Z)/max(B)*(Y-1.13*Y-1),Y))
Nccorr=Nc*(1+Nq/Nc*BL)*(1+0.4*ifelse(Z/B<=1,Z/B,atan(Z/B)))*(1-Beta/147)
Nqcorr=Nq*(1+BL*sin(fi*(pi/180)))*(1+2*tan(fi*(pi/180))*((1-sin(fi*(pi/180)))^2)*ifelse(Z/B<=1,Z/B,atan(Z/B)))*(1-0.5*tan(Beta*(pi/180)))^5
NYcorr_H=Ny_H*ifelse(1-0.4*BL<0,6,1-0.4*BL)*(1-0.5*tan(Beta*(pi/180)))^5
NYcorr_V=Ny_V*ifelse(1-0.4*BL<0,6,1-0.4*BL)*(1-0.5*tan(Beta*(pi/180)))^5
qu_H= C*Nccorr+q*Nqcorr+ypri*B*NYcorr_H/2
qu_V= C*Nccorr+q*Nqcorr+ypri*B*NYcorr_V/2
qa_H=qu_H/FS
qa_V=qu_V/FS
df_H<-data.frame(B,Nccorr,Nqcorr,NYcorr=NYcorr_H,qu=qu_H,qa=qa_H)
df_V<-data.frame(B,Nccorr,Nqcorr,NYcorr=NYcorr_V,qu=qu_V,qa=qa_V)

if(FSB==TRUE){
  fi<-atan(tan(fi*(pi/180))/FS)*(180/pi)
  C<-C/FS
  Nq=exp(pi*tan(fi*(pi/180)))*tan(pi/4+(fi/2)*(pi/180))^2
  Nc=ifelse(fi==0,5.14, (Nq-1)/tan(fi*(pi/180)))
  Ny_H=1.5*(Nq-1)*tan(fi*(pi/180))
  Ny_V=2*(Nq+1)*tan(fi*(pi/180))
  
  Nccorr=Nc*(1+Nq/Nc*BL)*(1+0.4*ifelse(Z/B<=1,Z/B,atan(Z/B)))*(1-Beta/147)
  Nqcorr=Nq*(1+BL*sin(fi*(pi/180)))*(1+2*tan(fi*(pi/180))*((1-sin(fi*(pi/180)))^2)*ifelse(Z/B<=1,Z/B,atan(Z/B)))*(1-0.5*tan(Beta*(pi/180)))^5
  NYcorr_H=Ny_H*ifelse(1-0.4*BL<0,6,1-0.4*BL)*(1-0.5*tan(Beta*(pi/180)))^5
  NYcorr_V=Ny_V*ifelse(1-0.4*BL<0,6,1-0.4*BL)*(1-tan(Beta*(pi/180)))^2
  
  qa_H=C*Nccorr+q*Nqcorr+ypri*B*NYcorr_H/2
  qa_V=C*Nccorr+q*Nqcorr+ypri*B*NYcorr_V/2
  
  df_H<-data.frame(B,Nccorr,Nqcorr,NYcorr= NYcorr_H,qu=qu_H,qa=qa_H)
  df_V<-data.frame(B,Nccorr,Nqcorr,NYcorr= NYcorr_V,qu=qu_V,qa=qa_V)
}

##################################################################


if(Cap_p=="Hansen" & fig==FALSE){ 
return(df_H)
}

if(Cap_p=="Vesic" & fig==FALSE){ 
  return(df_V)
}

df<-full_join(df_H,df_V)
df$cap<-c(rep("Hansen",nrow(df_H)),rep("Vesic",nrow(df_V)))

if(fig==TRUE){
library(ggplot2)
library(ggsci)

p<- ggplot(df, aes(x=B)) + 
  geom_line(aes(y=qu, col=cap))+
 
  labs( title = "Capacidad portante", 
        subtitle = "",
        x = "Longitud de la Base", 
        y = "Capacidad de carga",
        caption = "" ) + theme_minimal()

return(p)
}
########################################################
  }

capacidad_carga(FS=3,Cap_p="Vesic")
capacidad_carga(FS=3,Cap_p="Hansen")

capacidad_carga(FS=1.5,Cap_p="Hansen",FSB = TRUE)
capacidad_carga(FS=1.5,Cap_p="Vesic",FSB = TRUE)

capacidad_carga(FS=1.5,Cap_p="Hansen",fig = FALSE)





