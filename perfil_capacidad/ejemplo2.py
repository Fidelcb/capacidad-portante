
from rpy2.robjects import r
import rpy2.robjects as ro
import pandas as pd
import numpy as np
from pandas.core.indexes.base import Index

from pandas import DataFrame
from rpy2.robjects import pandas2ri

from rpy2.robjects.conversion import localconverter

df=pd.ExcelFile('2. Av. Dorado Definicion de Parámetros MC V0.xlsx')

#print(df.sheet_names)
df=df.parse('dorado')


#Perfil de suelo
def PerfilSuelo(df,max_E=5,R='FALSE',G='TRUE',name_ggplot='Gráfica',figure='FALSE'):

      r.assign('G', G)
      r.assign('R', R)
      r.assign('name_ggplot', name_ggplot)
      r.assign('figure', figure)
      r.assign('max_E', max_E)

      na= pd.isnull(df['U.S.C.'])
      pos=np.where(na)[0].tolist()     
      df1= df.drop(pos,axis=0)  
     

      with localconverter(ro.default_converter + pandas2ri.converter):
        df = ro.conversion.py2rpy(df1)

      r.assign('df', df)

     
      Names=['LL','LP','IP','Gravas','Arenas','Finos','U.S.C.']   
     
      r.assign('Names', Names)  
     
      
      r('''
      require(tidyverse)
      log=all(Names%in%names(df))
      ''')

      log=r('log')
      log2=log
      r.assign('log', log)  

      r('''

      if(log=='TRUE'){  

      df$LL<-as.numeric(df$LL)

      df$LP<-as.numeric(df$LP)

      df$IP<-as.numeric(df$IP)

      df$Gravas<-as.numeric(df$Gravas)

      df$Arenas<-as.numeric(df$Arenas)

      df$Finos<-as.numeric(df$Finos)

      df$w<-as.numeric(df$w)

      df<-df%>%mutate(Cc=ifelse(df$LL>=1,0.009*(df$LL-10),NA),
                      eo=2.4*df$w/100) 
      df$U.S.C.<-ifelse(df$U.S.C.=="CL", 1, ifelse(df$U.S.C.=="ML",2,ifelse(df$U.S.C.=="MH",3,
                                                                               ifelse(df$U.S.C.=="SC-SM",4,ifelse(df$U.S.C.=="SC-SM",5,ifelse(df$U.S.C.=="CH",6,
                                                                                                                                                    ifelse(df$U.S.C.=="SM",7,ifelse(df$U.S.C.=="ML-OH",8,ifelse(df$U.S.C.=="ML-OL",9,
                                                                                                                                                                                                                      ifelse(df$U.S.C.=="SC",10,ifelse(df$U.S.C.=="SC-SW",11,ifelse(df$U.S.C.=="SM-SW",12,
                                                                                                                                                                                                                                                                                          ifelse(df$U.S.C.=="SC-SP",13,ifelse(df$U.S.C.=="SM-SP",14,ifelse(df$U.S.C.=="GC-GM",15,
                                                                                                                                                                                                                                                                                                                                                                 ifelse(df$U.S.C.=="GC",16,ifelse(df$U.S.C.=="GC-GW",17,ifelse(df$U.S.C.=="GM-GW",18,
                                                                                                                                                                                                                                                                                                                                                                                                                                     ifelse(df$U.S.C.=="GC-GP",19,ifelse(df$U.S.C.=="GM-GP",20,ifelse(df$U.S.C.=="GW",21,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ifelse(df$U.S.C.=="GP",22,23))))))))))))))))))))))           
      df[is.na(df)] <- 1*10^-6

      library(nnet)
       dat_m<-df%>%select(c("LL","LP","IP","Gravas","Arenas","Finos","U.S.C."))

       Modelo<-multinom(U.S.C.~., data=dat_m)
       df<-df%>%mutate(Pred=as.numeric(as.vector(predict(Modelo))))
       D_M<-0.5*(df$inicio+df$fin)

       a<-duplicated(D_M)
       b<-which(a=="TRUE")
       j<-0.1
       for (i in 1:length(b)){
       D_M[b[i]]<-D_M[b[i]]+j
       j<-j+0.1
       }


       df<-df%>%mutate(D_M)
       df<- df[order(df$D_M), ]



       D_Inter<-NULL
       D_Acu<-NULL
       Pred_M<-NULL
       A_Inter<-NULL
       A_Acu<-NULL


       for (i in 2:nrow(df)) {
       D_Inter[1]<-0
       D_Acu[1]<-D_Inter[1]
       Pred_M[1]<-df$Pred[1]
       A_Inter[1]<-0
       A_Acu[1]<-A_Inter[1]
  
       D_Inter[i]<-df$fin[i]-df$inicio[i]
       D_Acu[i]<-D_Inter[i]+D_Acu[i-1]
       Pred_M[i]<-0.5*(df$Pred[i]+df$Pred[i-1])
       A_Inter[i]<-D_Inter[i]*Pred_M[i]
       A_Acu[i]<-A_Inter[i]+A_Acu[i-1]
       }


       Zx<-NULL
       for (i in 1:nrow(df)) {
       Zx[i]<-A_Acu[i]-((max(A_Acu)/max(D_Acu))*D_Acu[i])

       }


       df<-df%>%mutate(D_Inter,D_Acu,Pred_M,A_Inter, A_Acu,Zx)




       library(kmlShape)
       j=seq(0.1,max_E,0.1)
       DPE<-list()
       for (i in 1:length(j)) {
       DPE[[i]]<- DouglasPeuckerEpsilon(df$D_M,df$Zx,j[i]) 
       }
 
       d<-NULL
       for (i in 1:length(j)) {
       d[i]<-nrow(DPE[[i]])
       }

       f<-as.data.frame.array(table(d))

      logico<-max(f[,1])==f[,1]
      pos<-which(logico==TRUE)

      dim<-rownames(f)[pos] 
      B<-data.frame(j,d)
      B<-B%>%filter(B$d==dim)
      Epsilon=min(B$j)
      DPE<- DouglasPeuckerEpsilon(df$D_M,df$Zx,Epsilon)




       j<-2
       Group<-NULL
       for (i in 2:nrow(DPE)){
       b<-df$D_M>=DPE[i-1,1] & df$D_M<=DPE[i,1]
       Group[which(b==TRUE)]<-j-1
       j<-j+1
       }
  
       d=as.data.frame.array(table(Group,df$U.S.C.)) 


       for (i in 1:nrow(d)) {
       d[i,]<-round(prop.table(d[i,])*100,2)
       }

       inter<-DPE[,1]
       Inicio<-rep(0,nrow(d))
       Fin<-rep(0,nrow(d))


       for (i in 1:nrow(d)) {
       Inicio[i]<-round(inter[i],2)
       Fin[i]<-round(inter[i+1],2)
       }

       k<-data.frame(Inicio,Fin,d)
       Lista<-list(CC=tapply(df$Cc,Group, mean),
              e0=tapply(df$eo,Group, mean),
              W=data.frame(Min=tapply(df$w,Group, min),Max=tapply(df$w,Group, max)),
              WL=data.frame(Min=tapply(df$LL,Group, min),Max=tapply(df$LL,Group, max)),
              WP=data.frame(Min=tapply(df$IP,Group, min),Max=tapply(df$IP,Group, max)),
              Y=data.frame(Min=tapply(df$Y,Group, min),Max=tapply(df$Y,Group, max)) )
       }
       


       if(figure==TRUE){
       library(ggplot2)
       library(ggsci)

       p<- df%>%ggplot(aes(x =D_M, y = Zx)) +
       geom_line(data = DPE,aes(x =x, y = y),color="red")+
       geom_line(size = 1)+
       labs( title = name_ggplot, 
        subtitle = "Definición de Estratos",
        x = "Profundidad Promedio", 
        y = "f(x)",
        caption = "" ) +theme_minimal()


       }

       
       ''')

      k=r('k') 
      r.assign('k', k)
      if(G=='TRUE'):             
          return(k)
      
      Lista=r('Lista')
      r.assign('Lista', Lista)

     
      

      if(R=='TRUE'): 
            r('''

            cat("0.0m","a",paste0(inter[1],"m:"),"Capa de relleno granular","\n")
            cat("------------------------------------------\n")
            for (i in 1:nrow(k)) {
            US<- max(d[i,])==d[i,]
            U.S.C<-names(d[which(US==TRUE)])
  
            cat("Grupo",paste(i,":"),paste0(k$Inicio[i],"m -"),paste0(k$Fin[i],"m"),"\n")
  
            cat("CC:",Lista$CC[i]," e0:",Lista$e0[i],paste0(" U.S.C:",U.S.C)," W:",Lista$W[i,1],"-",Lista$W[i,2]," WL:",Lista$WL[i,1],"-",Lista$WL[i,2]," WP:",Lista$WP[i,1],"-",Lista$WP[i,2]," Y:",Lista$Y[i,1],"-",Lista$Y[i,2],"\n")
            cat("----------------------------------------------------------\n")
            }
            ''')
      
      
      if(figure=='TRUE'):
            p=r('p')
            return p
            
            

print(PerfilSuelo(df=df))






#Capacidad portante

def cap(C=0.25,fi=29.4,Y=1.81,Z=1.2,NF=5,Beta=0,fib=1,BL=0.75,FS=1.5,Cap_p='Vesi',FSB='FALSE',fig='TRUE'):
      
      r.assign('FSB', FSB)
      r.assign('fig', fig)
      r.assign('Cap_p', Cap_p)
      pi=r.pi
      r.assign('C', C)
      r.assign('fi', fi)
      r.assign('NF', NF)
      r.assign('Y', Y)
      r.assign('Z', Z)
      r.assign('BL', BL)
      r.assign('fib', fib)
      r.assign('FS', FS)
      r.assign('Beta', Beta)

      q=r.ifelse(NF<Z,NF*Y+(Z-NF)*(1.13*Y-1),Y*Z)
      r.assign('q', q)

      Nq=r('exp(pi*tan(fi*(pi/180)))*tan(pi/4+(fi/2)*(pi/180))^2')
      r.assign('Nq', Nq)

      Nc=r('ifelse(fi==0,5.14, (Nq-1)/tan(fi*(pi/180)))')
      r.assign('Nc', Nc)
      Ny_H=r('1.5*(Nq-1)*tan(fi*(pi/180))')
      r.assign('Ny_H', Ny_H)
      Ny_V=r('2*(Nq+1)*tan(fi*(pi/180))')
      r.assign('Ny_V', Ny_V)
      
      r('''
      B<-NULL
      B[1]<-1
      i=1
      while (B[i]<3.5) {
        B[i+1]<-B[i]+0.25
         i<-i+1

         }
      
      ''')
      B=r('B')
      
      a=r.ifelse(NF-Z<max(B),1.13*Y-1+(NF-Z)/max(B)*(Y-1.13*Y-1),Y)
      r.assign('a', a)
      ypri=r.ifelse(NF<Z,1.13*Y-1,a)
      r.assign('ypri', ypri)

      Nccorr=r('Nc*(1+Nq/Nc*BL)*(1+0.4*ifelse(Z/B<=1,Z/B,atan(Z/B)))*(1-Beta/147)')
      r.assign('Nccorr', Nccorr)
      Nqcorr=r('Nq*(1+BL*sin(fi*(pi/180)))*(1+2*tan(fi*(pi/180))*((1-sin(fi*(pi/180)))^2)*ifelse(Z/B<=1,Z/B,atan(Z/B)))*(1-0.5*tan(Beta*(pi/180)))^5')
      r.assign('Nqcorr', Nqcorr)
      NYcorr_H=r('Ny_H*ifelse(1-0.4*BL<0,6,1-0.4*BL)*(1-0.5*tan(Beta*(pi/180)))^5')
      r.assign('NYcorr_H', NYcorr_H)
      NYcorr_V=r('Ny_V*ifelse(1-0.4*BL<0,6,1-0.4*BL)*(1-0.5*tan(Beta*(pi/180)))^5')
      r.assign('NYcorr_V', NYcorr_V)
      qu_H= r('C*Nccorr+q*Nqcorr+ypri*B*NYcorr_H/2')
      r.assign('qu_H', qu_H)    
      qu_V= r('C*Nccorr+q*Nqcorr+ypri*B*NYcorr_V/2 ')
      r.assign('qu_V', qu_V)
      qa_H=r('qu_H/FS')
      r.assign('qa_H', qa_H)
      qa_V=r('qu_V/FS')
      r.assign('qa_V', qa_V)
      df_H=r('data.frame(B,Nccorr,Nqcorr,NYcorr=NYcorr_H,qu=qu_H,qa=qa_H)')
      r.assign('df_H', df_H)
      df_V=r('data.frame(B,Nccorr,Nqcorr,NYcorr=NYcorr_V,qu=qu_V,qa=qa_V)')
      r.assign('df_V', df_V)

      #####################################
      r('''
      if(FSB==TRUE){
      fi<-atan(tan(fi*(pi/180))/FS)*(180/pi)
      C<-C/FS
      Nq=exp(pi*tan(fi*(pi/180)))*tan(pi/4+(fi/2)*(pi/180))^2
      Nc=ifelse(fi==0,5.14, (Nq-1)/tan(fi*(pi/180)))
      Ny_H=1.5*(Nq-1)*tan(fi*(pi/180))
      Ny_V=2*(Nq+1)*tan(fi*(pi/180))
  
      Nccorr=Nc*(1+Nq/Nc*BL)*(1+0.4*ifelse(Z/B<=1,Z/B,atan(Z/B)))*(1-Beta/147)
      Nqcorr=Nq*(1+BL*sin(fi*(pi/180)))*(1+2*tan(fi*(pi/180))*((1-sin(fi*(pi/180)))^2)*ifelse(Z/B<=1,Z/B,atan(Z/B)))*(1-0.5*tan(Beta*(pi/180)))^5
      NYcorr_H=Ny_H*ifelse(1-0.4*BL<0,6,1-0.4*BL)*(1-0.5*tan(Beta*(pi/180)))^5
      NYcorr_V=Ny_V*ifelse(1-0.4*BL<0,6,1-0.4*BL)*(1-tan(Beta*(pi/180)))^2
  
      qa_H=C*Nccorr+q*Nqcorr+ypri*B*NYcorr_H/2
      qa_V=C*Nccorr+q*Nqcorr+ypri*B*NYcorr_V/2
  
      df_H1<-data.frame(B,Nccorr,Nqcorr,NYcorr= NYcorr_H,qu=qu_H,qa=qa_H)
      df_V1<-data.frame(B,Nccorr,Nqcorr,NYcorr= NYcorr_V,qu=qu_V,qa=qa_V)
      }
      ''')

      if(FSB=='TRUE'):
         df_H=r('df_H1')
         df_V=r('df_V1')

##################################################################
      
      if(Cap_p=="Hansen" and fig=='FALSE'):
         return(df_H)

      if(Cap_p=="Vesic" and fig=='FALSE'):
            return(df_V)        

      r('''
      require(tidyverse)
      df<-full_join(df_H,df_V)
      df$cap<-c(rep("Hansen",nrow(df_H)),rep("Vesic",nrow(df_V)))
      ''')
      df=r('df')

      r('''

      if(fig==TRUE){
      library(ggplot2)
      library(ggsci)

      p<- ggplot(df, aes(x=B)) + 
                geom_line(aes(y=qu, col=cap))+ 
                labs( title = "Capacidad portante", 
                 subtitle = "",
                x = "Longitud de la Base", 
                y = "Capacidad de carga",
                caption = "" ) + theme_minimal()      
      }
      ''')
      
      if(fig=='TRUE'):
            p=r('p')
            return p
            
      
#print(cap())



#Asentamiento
## Librerias 
#require(tidyverse)
#require(readxl)

## Lectura de la base de datos
##########################################################################

df=pd.ExcelFile('9.3 Inducel INSE.xlsx')

#print(df.sheet_names)
ds=df.parse('Servicio')
du=df.parse('Ultima')

#Resorte inicial
def r_inicial(ds,du,Sadm=5.44,Smax=8.16,d=2):

      Pz=round(0.1*Ultima$F3,4)  
      Area_Neta<-round((Ultima$F3+Pz)/(Smax*100),4)
      lado_neto<-round(Area_Neta^(0.5),4)
      ex<-round((Servicio$M1)/(Servicio$F3),4)
      ey<-round((Servicio$M2)/(Servicio$F3),4)
      Bz<-2*(lado_neto/2+abs(ex))
      Lz<-2*(lado_neto/2+abs(ey))
      B<-c(2.60,2.00,2.60,2.00, 4.20,4.20,4.20,4.20,1.80,1.80)
      L<-B
      Kz<-(Sadm/d)*B*L*10000
      Kxx<-(B*(L^3)/12)*(Sadm/d)*10000
      Kyy<-(L*(B^3)/12)*(Sadm/d)*10000

      Resorte_Inicial<-data.frame(Pz,Area_Neta,lado_neto,ex,ey,Bz,Lz,B,L,Kz,Kxx,Kyy)
      return(Resorte_Inicial)

        }

Resorte_Inicial=Res_Inicial(servicio,Ultima) 

#Resorte_Inicial= rename(Resorte_Inicial, c("Pz KN"="Pz_KN"))

# Asentamiento
Asen<-function(Resorte_Inicial,Ultima,Fact_X=4,es=52500,u=0.4,A=0.5,r=FALSE,ZII=FALSE,DZI=FALSE){
  suppressMessages(attach(Ultima))
  suppressMessages(attach(Resorte_Inicial))
  
  Lx<-B/2
  Bx<-L/2
  q<-F3/(B*L)
  
  n=nrow(Ultima)
  Z<-rep(1,n)
  DSz<-NULL
  DSz_<-rep(10,n)
  R1<-NULL
  R2<-NULL
  R3<-NULL

i<-1

for (j in 1:n) {
while (!DSz_[i]<10) {
      R1[i]<-(Lx[i]^2+Z[i]^2)^0.5
      R2[i]<-(Bx[i]^2+Z[i]^2)^0.5
      R3[i]<-(Lx[i]^2+Bx[i]^2+Z[i]^2)^0.5
      DSz[i]<-(Fact_X*(q[i]/(2*pi)))*(atan(Lx[i]*Bx[i]/(Z[i]*R3[i]))+((Lx[i]*Bx[i]*Z[i])/R3[i])*(1/(R1[i]^2)+1/(R2[i]^2)))
      DSz_[i]<-(DSz[i]/q[i])*100
      
      Z[i]<-Z[i]+1
    }
i<-i+1
}

if (r==TRUE) {
return(R2)
}
Z<-Z-1

ng<-c()
for (i in 1:length(Z)) {
  ng[i]=length(seq(0,Z[i],A)[-1])
}


lista<-list()
lista2<-list()
for (j in 1:n) {
  Zi<-NULL
  Zi[1]<-A/2
  
for (i in 2:ng[j]) {
    Zi[i]<-Zi[i-1]+A
}

lista2[[j]]<-Zi


hj<-rep(A,ng[j])

DSx<-(Fact_X*(q/(2*pi)))*(atan(Lx*Bx/(Z*R3))-((Lx*Bx*Z)/((R1^2)*R3)))
DSy<-DSx

R1j<-NULL
for (i in 1:ng[j]) {
  R1j[i]<-(Lx[j]^2+Zi[i]^2)^(0.5)
}

R2j<-NULL
for (i in 1:ng[j]) {
  R2j[i]<-(Bx[j]^2+Zi[i]^2)^(0.5)
}

R3j<-NULL
for (i in 1:ng[j]) {
  R3j[i]<-(Lx[j]^2+Bx[j]^2+Zi[i]^2)^(0.5)
}

DSz_i<-NULL
DSx_i<-NULL 

for (i in 1:ng[j]){
DSz_i[i]<-(Fact_X*(q[j]/(2*pi)))*(atan(Lx[j]*Bx[j]/(Zi[i]*R3j[i]))+((Lx[j]*Bx[j]*Zi[i])/R3j[i])*(1/(R1j[i]^2)+1/(R2j[i]^2)))
DSx_i[i]<-(Fact_X*(q[j]/(2*pi)))*(atan(Lx[j]*Bx[j]/(Zi[i]*R3j[i]))-(Lx[j]*Bx[j]*Zi[i])/((R2j[i]^2)*R3j[i]))

}  
DSy_i<-DSx_i
  
  
###
DZi<-NULL
for (i in 1:ng[j]) {
  DZi[i]<-((DSz_i[i]/es)-((u/es)*(DSx_i[i]+DSy_i[i])))*hj[i]
}
  
  
Dz_total_m<-sum(DZi)
  
Dz_total_mm<-Dz_total_m*1000
  
D=as.data.frame(matrix(DZi,nrow = 1))
names(D)<-paste0(rep("DZ",ng[j]),1:ng[j])
  
lista[[j]]<-data.frame(D,Dz_total_m,Dz_total_mm)
}
if(ZII==TRUE){
  return(lista2)
}

  return(lista)



  }

Asentamiento<-Asen(Resorte_Inicial,Ultima)


#Resorte definitivo

def<-function(Asentamiento,Resorte_Inicial,Sadm=5.44){
  suppressMessages(attach(Resorte_Inicial))
  R2<-Asen(Resorte_Inicial,Ultima,r=TRUE)
  pz<-0.1*R2
  
  dz<-NULL
  for (i in 1:length(R2)) {
    dz[i]<-Asentamiento[[i]]$Dz_total_m
  }
  
  Kz<-(Sadm/dz)*B*L*100
  
  return(data.frame(Asentamiento=dz,pz,Area_Neta,lado_neto,ex,ey,Bz,Lz,Kxx,Kyy))
}

Resortes_definitivo<-def(Asentamiento,Resorte_Inicial)

